VERSION=

summary:
	python makewebpage.py > web$(VERSION)/index.html
	python concatregions.py
	python plotmasks.py
regions:
	python disp2.py all parallel
package:
	rm -f web/vipersmask.tar.gz
	tar cvzf ../vipersmask.tar.gz --exclude='data/*' ../vipersmask/ 
	mv ../vipersmask.tar.gz web
plots:
	plotmask.py --save plots/fields_W1.png regions/mask_W1*.reg
	plotmask.py --save plots/fields_W4.png regions/mask_W4*.reg

	python masklist.py -f W1 -b regions/mask_%s.reg | plotmask.py  --save plots/fields_W1_1.1.png
	python masklist.py -f W4 -b regions/mask_%s.reg | plotmask.py  --save plots/fields_W4_1.1.png
mangle:
	#ds9_to_mangle.py mask_W*.reg
	#./scripts/makemasks.sh

webproducts:
	tar cvzf ../vipersweb/regions.tar.gz regions
	tar cvzf ../vipersweb/manglemasks.tar.gz manglemasks
	tar cvzf ../vipersweb/masks1.1.tar.gz masks1.1
	tar cvzf ../vipersweb/masks1.1_cut.tar.gz masks1.1_cut

clean:
	rm -f *~ *pyc
