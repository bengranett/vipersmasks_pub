import sys,os
import numpy as N
import pylab
import pyfits
import ds9
import time
from kapteyn import wcs


import findedges

version = ''
datadir = 'data'
#cfhtregfile = '/home/ben/data/vipers/VIPERS_masks/masks/CFHTLS_%s_i_T0005_Jean.reg'
quads = ['Q1','Q2','Q4','Q3']


D = None
def startds9():
    """ Start the ds9 instance.
    This uses the python bindings for XPA.  information is here:
    http://hea-www.harvard.edu/saord/ds9/pyds9/
    """
    global D
    D = ds9.ds9("%f"%N.random.uniform())  # ensures we open a new window
    D.set('view layout vertical')
    D.set('view buttons no')
    D.set('view magnifier no')
    D.set('view colorbar no')
    D.set('view panner no')
    D.set('view info no')

    D.set('height 900')
    D.set('width 900')
    D.set('prefs bgcolor green')
    return D


def catalogtoregions(path):
    """ """
    outfile = "%s.reg"%(path)
    if os.path.exists(outfile):
        return outfile
    out = file(outfile,'w')
    i=0

    print >>out, "global color=magenta"
    print >>out, "image"
    for line in file(path):
        i+=1
        if i==1: continue
        w = line.split()
        ra,dec = w[1:3]
        print >>out, "point %s %s # point=circle"%(ra,dec)
    out.close()
    return outfile

def spectargets(pointing,quad,dir='tmp/specregfiles'):
    if not os.path.exists(dir):
        os.system('mkdir -pv %s'%dir)
    outfile = '%s/spec_%s%s.reg'%(dir,pointing,quad)
    outfile2 = '%s/specpoor_%s%s.reg'%(dir,pointing,quad)

    if os.path.exists(outfile2):
        return outfile,outfile2
    field = pointing[:2]
    fits = pyfits.open('data_1.1/%sspec.fits'%field)

    ra = fits[1].data.field('%s_PHOT_alpha'%field)
    dec = fits[1].data.field('%s_PHOT_delta'%field)
    points = fits[1].data.field('%s_SPECTRO_V1_1_pointing'%field)
    quads = fits[1].data.field('%s_SPECTRO_V1_1_quadrant'%field)
    flag = fits[1].data.field('%s_SPECTRO_V1_1_zflg'%field)

    print points
    print quads

    out = file(outfile,'w')
    out2 = file(outfile2,'w')

    print >>out, "global color=cyan"
    print >>out, "FK5"
    print >>out2, "global color=magenta"
    print >>out2, "FK5"

    
    for i in range(len(ra)):
        if points[i] != pointing: continue
        if quads[i] != int(quad[1]): continue
        print >>out, "point %s %s # point=circle"%(ra[i],dec[i])
        if flag[i]<1:
            print >>out2, "point %s %s # point=circle"%(ra[i],dec[i])

    out.close()
    out2.close()
    return outfile,outfile2

def checkpointing(pointing, skipdone=False):
    """checks if pointing is valid and if it has already been done."""
    try:
        assert(pointing.startswith("W") and len(pointing)==6)
    except:
        return False

    if skipdone:
        if os.path.exists('regions%s/mask_%sQ1.reg'%(version,pointing)):
            return False
    
    return True

    
def go(pointing):
    """ Start processing pointing. """
    if not checkpointing(pointing):
        print "> skipping",pointing
        return

    # the data is probably in the data directory as a tar.gz file
    dir = '%s/%s'%(datadir,pointing)
    targz = '%s/%s.tar.gz'%(datadir,pointing)
    diduntar=0
    if not os.path.exists(dir):
        if os.path.exists(targz):
            print "> found %s, will untar"%targz
            os.system("mkdir %s"%dir)
            os.system("tar xvzf %s -C %s"%(targz,dir))
            diduntar=1
        else:
            print "> can't find data at %s, continuing"%dir
            return

    # field is W1 or W4
    field = pointing[:2]
    #cfhtregfile2 = cfhtregfile%field   # decided we dont need this

    outdir = 'regions%s'%version
    if not os.path.exists(outdir):
        os.mkdir(outdir)   # make directory structure for web page
        os.system('mkdir -p web%s/images'%version)
        os.system('mkdir -p web%s/smallimages'%version)
    
    for i,Q in enumerate(quads):  # loop over the 4 quadrants
        tag = pointing+Q
        path = '%s/%s/%s.fits'%(datadir,pointing,tag)
    
        fits = pyfits.open(path)

        # run the algorithm to find the image boundaries
        regfile = findedges.makeregions(fits,tag,outdir=outdir)

        # show results in DS9
        D.set('frame %i'%(i+1))
        D.set('file %s'%path)
        D.set('zscale')
        D.set('zoom to fit')

        print "region file",regfile
        D.set('regions load %s'%regfile)
        D.set('regions selectall')
        D.set('regions width 4')
        D.set('regions selectnone')

        ### skip overplotting the photometric mask
        #print "loading",cfhtregfile2
        #D.set('regions load %s'%cfhtregfile2)
        # mark targets
        #D.set('regions load %s'%catalogtoregions("%s/%s_cross_obj.txt"%(pointing,tag)))
        
        # do mark the spectroscopic targets
        targ1,targ2=spectargets(pointing,Q)
        D.set('regions load %s'%targ2)
        D.set('regions load %s'%targ1)

        # add a big text label
        tmp = file('tmpregionfile','w')
        print >>tmp, "text 1000 1650 # text={%s} font=\"helvetica 36\" color=white"%(pointing)
        print >>tmp, "text 1000 1500 # text={%s} font=\"helvetica 36\" color=white"%(Q)
        tmp.close()
        D.set('regions load %s'%'tmpregionfile')

        # we are done, take a screen shot and do some image conversions
        file1 = 'tmp/%s_%s.png'%(pointing,Q)
        file2 = 'web%s/images/%s_%s.gif'%(version,pointing,Q)
        thumb = 'web%s/smallimages/%s_%s_s.gif'%(version,pointing,Q)
        D.set('saveimage png %s'%(file1))
        os.system('convert -trim +repage %s %s'%(file1,file2))
        os.system('convert -scale 30%'+' %s %s'%(file2,thumb))

        os.unlink(file1)


    # if the data directory was originally compressed, delete what we unpacked
    if diduntar:
        os.system('rm -rf %s'%dir)


def doall(nproc=1):
    """ Process regions for all pointings in the data directory.
    nproc>1 will run multiple instances so you can process in parallel.
    """
    print "nproc",nproc
    pp = []
    for f in os.listdir(datadir):
        pointing = f[:6]
        if checkpointing(pointing):
            pp.append(pointing)
    print '> pointings to do:', pp
    n = len(pp)
    batches = [""]*nproc
    print batches
    j = 0
    for i in range(n):
        batches[j]+="%s "%pp[i]
        j += 1
        j = j%nproc
    print batches

    for j in range(nproc):
        cmd = "python %s %s&"%(sys.argv[0],batches[j])
        print cmd
        os.system(cmd)
        time.sleep(1)

if __name__=="__main__":
    if len(sys.argv)==1:
        sys.exit()
        
    pointings = sys.argv[1:]
    if 'all' in pointings:

        if 'parallel' in pointings:
            procs = 4
        else:
            procs = 1
        doall(nproc=procs)
        sys.exit()

    startds9()

    for p in pointings:
        go(p)
        
    D.set('exit')


