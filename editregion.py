import sys,os
import numpy as N
import pylab
import pyfits
import ds9
import time
from kapteyn import wcs
from scipy import ndimage
import disp2
import findedges

version = ''
datadir = 'data'
cfhtregfile = '/home/ben/data/vipers/VIPERS_masks/masks/CFHTLS_%s_i_T0005_Jean.reg'
quads = ['Q1','Q2','Q4','Q3']


D = None
def startds9():
    """ """
    global D
    D = ds9.ds9("%f"%N.random.uniform())
    D.set('view layout vertical')
    D.set('view buttons no')
    D.set('view magnifier no')
    D.set('view colorbar no')
    D.set('view panner no')
    D.set('view info no')

    D.set('height 900')
    D.set('width 900')
    D.set('prefs bgcolor green')
    return D


def go(pointing,quad):
    """ """
    dir = '%s/%s'%(datadir,pointing)
    targz = '%s/%s.tar.gz'%(datadir,pointing)
    diduntar=0
    if not os.path.exists(dir):
        if os.path.exists(targz):
            print "> found %s, will untar"%targz
            os.system("mkdir %s"%dir)
            os.system("tar xvzf %s -C %s"%(targz,dir))
            diduntar=1
        else:
            print "> can't find data at %s, continuing"%dir
            return

    field = pointing[:2]

    outdir = 'regions%s'%version
    

    tag = pointing+quad
    path = '%s/%s/%s.fits'%(datadir,pointing,tag)
    
    fits = pyfits.open(path)

    regfile = findedges.makeregions(fits,tag,outdir=outdir)

    backup = "%s.backup"%regfile
    if not os.path.exists(backup):
        os.system("cp %s %s"%(regfile,backup))

    D.set('file %s'%path)
    D.set('zscale')
    D.set('zoom to fit')

    print "region file",regfile
    D.set('regions load %s'%regfile)

    D.set('regions selectall')
    D.set('regions width 1')
    D.set('regions coord fk5')
    D.set('regions format ds9')

    while True:
        print "> Are you done editing the region? (y,q)",
        sys.stdout.flush()
        input = sys.stdin.readline()
        input = input.strip()
        if input=='q' or input == '':
            print "exiting without saving."
            break
        if input=='y':
            print "saving region to %s"%regfile
            D.set('regions save %s'%regfile)

            break
        print "type y or q"

        


    # if the data directory was originally compressed, delete what we unpacked
    if diduntar:
        os.system('rm -rf %s'%dir)


if __name__=="__main__":
    startds9()
    pointing = 'W4P021'
    quad = 'Q1'
    while True:
        print "enter pointing and quadrant: [%s %s]"%(pointing,quad),
        sys.stdout.flush()
        input = sys.stdin.readline()
        if input in ["","q","Q"]: break
        

        w = input.split()
        if not input.strip() == "":
            if len(w) != 2:
                print "   I don't understand",input
                continue
            pointing,quad = w
        if not disp2.checkpointing(pointing):
            print "   is %s a valid pointing?"%pointing
            continue

        if len(quad)==1:
            quad = 'Q'+quad
            
        try:
            assert(quad[0]=='Q' and quad[1] in ['1','2','3','4'])
        except:
            print "quad %s is not valid"%quad
            continue

        go (pointing,quad)
    print
        
    print "> finito <"

