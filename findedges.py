import sys,os
import numpy as N
import pylab
import pyfits
import time
from kapteyn import wcs
from scipy import ndimage
from scipy import stats
import cPickle as pickle

import simplify
#import fitline

def stat(image, iterations=5, cut=2,filter=None,axis=None,verbose=False):
    """ Measure statistics in an image.
    Returns:
      a tuple with
      mean, standard deviation, skewness and number of pixels.
      (skewness is set to 0 and not used currently)
    Inputs:
      image: an array to measure statistics on
      iterations: number of iterations to do
      cut: number of standard deviations used to remove outliers.
      filter: a binary mask.  If specified, only elements
              with filter==1 will be used.
      verbose: print out stats at each iteration.
    """
    imgt = image.copy()
    if not filter==None:
        assert(N.all(filter.shape==image.shape))
        mask = N.where(filter==1)
        imgt = imgt[mask]
    
    for i in range(iterations):
        m = N.median(imgt,axis=axis)
        sig = N.std(imgt,axis=axis)
        #skew = stats.skew(imgt)
        skew = 0
        n = len(imgt)
        if verbose:
            print "> %i: %f %f %f %i"%(i,m,sig,skew,n)
        
        ii = N.where(N.abs(imgt-m)/sig<=cut)
        imgt = imgt[ii]

    if verbose:
        print "> done %i: %f %f %f %i"%(i,m,sig,skew,n)

    return m,sig,skew,n

def norm(x):
    if x==0: return 0
    return x*1./N.abs(x)

def findedges(im,iterations=1000,b = 25,
              blocksize=1524,nsteps=31,lim=20,siglim=10,
              plot=False,eps=1e-10,speed=2,tag=""):
    """Big routine to process an image and find boundaries of
    the 'good' region.
    """
    if plot:
        pylab.imshow(N.log10(im),origin='lower')
    sy,sx = im.shape
    cx,cy = sx/2.,sy/2.


    imlp = ndimage.filters.laplace(im,mode='mirror')

    # define filter for measuring statistics in
    filter = N.fromfunction(lambda x,y:(x-b)**2+(y-b)**2<=b**2,(2*b+1,2*b+1))

    # get statistics
    stats = []
    statslp = []
    print cy-blocksize/2,cy+blocksize/2
    print cx-blocksize/2,cx+blocksize/2
    for i in N.arange(cy-blocksize/2,cy+blocksize/2,2*b):
        for j in N.arange(cx-blocksize/2,cx+blocksize/2,2*b):
            sub = im[i:i+2*b+1,j:j+2*b+1]
            sublp = imlp[i:i+2*b+1,j:j+2*b+1]
            stats.append(stat(sub,filter=filter))
            statslp.append(stat(sublp,filter=filter))

    m,s,s2,n = N.transpose(stats)
    ml,sl,s2l,nl = N.transpose(statslp)

    # pylab.figure()
    # pylab.hist(m,bins=N.arange(-10,10,.1))
    # pylab.figure()
    # pylab.hist(s,bins=N.arange(0,500,10))
    
    bg,bgsig,bgs2,n = stat(m, iterations=5, cut=2,verbose=True)
    sbg,sbgsig,sbgs2,n = stat(sl, iterations=5, cut=2,verbose=True)

    print "> background %g+-%g, sig %g+-%g, skew %g+-%g"%(bg,bgsig,sbg,sbgsig,bgs2,sbgs2)
    # pylab.show()
    # sys.exit()

    im -= bg       # subtract mean
    #im = N.abs(im) # take absolute value

    xx = N.linspace(b,sx-b,nsteps)
    yy = N.linspace(b,sy-b,nsteps)

    points = []
    ones = N.ones(len(xx))
    zeros = N.zeros(len(xx))
    points.append(zip(xx,zeros))
    points.append(zip((sx-1)*ones,yy))
    points.append(zip(xx[::-1],(sy-1)*ones))
    points.append(zip(zeros,yy[::-1]))
    points = N.vstack(points)

    x,y = N.transpose(points)

    print len(x)
    #x = [x[70]]
    #y = [y[70]]

    if plot:
        pylab.plot(x,y,".")

    #checkim = im*0.
    #checkim2 = im*0.+siglim


    for i in range(len(x)):
        for count in range(iterations):
            dx = x[i]-cx
            dy = y[i]-cy
            a = N.sqrt(dx**2+dy**2)
            if a <= 0:
                print dx,dy,a
                break
            assert(a>0)
            dx /= a
            dy /= a
            vx = norm(dx)
            vy = norm(dy)
            if abs(dx)<eps: vx,dx=0,0
            if abs(dy)<eps: vy,dy=0,0

            xx = int(N.ceil(x[i]))
            yy = int(N.ceil(y[i]))
            
            x0 = max(0,xx-b)
            x1 = min(sx-1,xx+b+1)
            y0 = max(0,yy-b)
            y1 = min(sy-1,yy+b+1)

            x0a = max(b-xx,0)
            x1a = min(2*b+1,sx-xx-b-1+2*b)
            y0a = max(0,b-yy)
            y1a = min(2*b+1,sy-(yy+b+1)+2*b)

            #print xx,yy,sx,sy,b
            #print x0,x1,y0,y1
            #print x0a,x1a,y0a,y1a
            assert(x1>x0)
            assert(y1>y0)
            assert(x1-x0==x1a-x0a)
            assert(y1-y0==y1a-y0a)

            filtermask = filter*N.fromfunction(lambda x,y: -(x-b)*dy - (y-b)*dx >=0, (2*b+1,2*b+1))

            #print xx,yy,dx,dy
            
            sub = im[y0:y1,x0:x1]
            sublp = imlp[y0:y1,x0:x1]
            subf = filtermask[y0a:y1a,x0a:x1a]

            # pylab.subplot(211)
            # pylab.imshow(sub,origin='lower')
            # pylab.colorbar()
            # pylab.subplot(212)
            # pylab.imshow(subf,origin='lower')
            # pylab.colorbar()
            # pylab.show()

            assert(N.all(sub.shape==subf.shape))
            # pylab.subplot(211)
            # pylab.imshow(sub)
            # pylab.subplot(212)
            # pylab.imshow(subf)
            # pylab.show()

            npix = N.prod(sub.shape)
            m,s,s2,n = stat(sub,filter=subf)
            ml,s,s2,n = stat(sublp,filter=subf)

            #print ">",xx,yy,count,"|",m,s,n,"|",(s-sbg)/sbgsig
            #checkim[y0:y1,x0:x1] = N.abs(s2-sbgs2)/sbgs2
            #checkim2[y0:y1,x0:x1] = N.min([siglim*3,N.abs(s-sbg)/sbgsig])

            if n < npix*0.1:
                print "warning only",n,npix

            #if N.abs(m)/bgsig<lim and
            if N.abs(m)/bgsig<lim and N.abs(s-sbg)/sbgsig<siglim:
                break


            if x[i] < y[i] and x[i] < sy-y[i] and x[i] < cx:
                dy,dx=0,norm(dx)
            elif sx-x[i] < y[i] and sx-x[i] < sy-y[i] and x[i] >= cx:
                dy,dx=0,norm(dx)
            elif x[i] >= y[i] and sx-x[i] >= y[i] and y[i]<cy:
                dx,dy=0,norm(dy)
            elif x[i] >= sy-y[i] and sx-x[i] >= sy-y[i] and y[i]>cy:
                dx,dy=0,norm(dy)
            else:
                print x[i]<y[i],x[i] < sy-y[i],x[i] < cx
                print sx-x[i] < y[i], sx-x[i] < sy-y[i], x[i] >= cx
                print x[i] >= y[i], sx-x[i] >= y[i], y[i]<cy
                print x[i] >= sy-y[i], sx-x[i] >= sy-y[i], y[i]>cy
                print "wtf",x[i],y[i]
                sys.exit()

            #print x[i],y[i],dx,dy

            assert(N.isfinite(dx))
            assert(N.isfinite(dy))
            x[i] -= dx*speed
            y[i] -= dy*speed

        if count>iterations-10:
            print "> ran out of iterations!",count

    #hdu = pyfits.PrimaryHDU(checkim)
    #hdu.writeto('tmp/checkim1_%s.fits'%tag,clobber=True)

    #hdu = pyfits.PrimaryHDU(checkim2)
    #hdu.writeto('tmp/checkim2_%s.fits'%tag,clobber=True)

    # round to nearest integers
    #x = N.round(x)
    #y = N.round(y)

    if plot:
        pylab.plot(x,y,"o")
        pylab.show()
    return x,y,im


def makeregions(fits,tag,outdir='regions',overwrite=False):
    """Run the findedges routine and write a regions file in ds9 format.
    Uses the WCS info in the fits header to convert pixel coordinates to
    RA and Dec."""
    
    fname = '%s/mask_%s.reg'%(outdir,tag)
    if os.path.exists(fname) and not overwrite:
        return fname
    
    im = fits[0].data
    Proj = wcs.Projection(fits[0].header)
    x,y,im = findedges(im,tag=tag)
    poly = N.transpose([x,y])
    pickle.dump(poly,file('polysave/poly%s.pickle'%tag,'w'))
    c = simplify.simplify(poly)
    x,y = N.transpose(c)
    print 'reduction %i => %i'%(len(poly),len(c))

    c = zip(x,y)
    bounds = Proj.toworld(c)
    print len(bounds)

    fname = '%s/mask_%s.reg'%(outdir,tag)
    out = file(fname,'w')

    print >>out, "global width=1 dash=1 color=red"
    print >>out, "FK5"
    string = "polygon("
    #print >>out, "polygon(",
    for c in bounds:
        string += "%f,%f,"%c
        #print >>out, "%f,%f,"%c,
    string = string[:-1]
    string += ")"
    print >>out, string
    #print >>out, "\b)"
    out.close()
    return fname


if __name__=="__main__":
    fits = pyfits.open(sys.argv[1])
    im = fits[0].data

    findedges(im,plot=True)

