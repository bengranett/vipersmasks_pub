import sys,os
import numpy as N
import fitline
import pylab


def removedups(poly,eps=400.):
    """ Remove neighboring points if separation is < sqrt(eps)"""
    i = 0
    out = []
    run = [poly[0]]
    while i<len(poly):
        if N.sum((poly[i]-N.mean(run,axis=0))**2)<eps:
            run.append(poly[i])
        else:
            out.append(N.mean(run,axis=0))
            run = [poly[i]]
        i+=1
    return N.array(out)

def trim(resid,eps=20):
    n = len(resid)
    start = 0
    end = 0
    for i in range(n):
        if N.abs(resid[i])>eps: continue
        if N.abs(resid[i+1])>eps: continue
        start = i
        break
    for i in range(n):
        if N.abs(resid[n-i-1])>eps: continue
        if N.abs(resid[n-i-2])>eps: continue
        end = n-i
        break
    return start,end

def findruns(poly,eps=100):
    """ """
    n = len(poly)
    out = []
    allruns = []
    runl = []
    fitstats = []
    for i in range(n):
        run = [poly[i]]
        runi = [i]
        inext = i
        iprev = i
        yfit = None
        prevf = None
        while True:
            if len(run)<3:
                inext = (inext+1)%n
                run.append(poly[inext])
                runi.append(inext)
                continue

            x,y = N.transpose(run)
            prevfit = yfit

            cut = 100
            iterations = 0
            if len(x) > 3:
                cut=3.
                iterations=1
            fit,yfit,resid,fitii = fitline.fitline(x,y,usefit=prevf,
                                                   iterations=iterations,
                                                   cut=cut)
            #print 'fit results',fit
            fitl = len(x[fitii])

            if not fit==None:
                prevf = (fit,N.std(resid[fitii]))
            if fit==None:
                #print x
                #print y
                #print 'no fit'
                break

            fitstd = N.std(resid[fitii])
            #fitl = len(resid[fitii])

            if fitstd>5:
                #print "crappy fit"
                yfit=None
                break

            #print fitii
            if len(x[fitii])< max(0.2*len(x),3):
                #print "too few",len(x[fitii])
                yfit=None
                break
            
            #pylab.figure()
            #pylab.plot(x,y,".")
            #pylab.plot(yfit[0],yfit[1])
            #pylab.show()

            if prevfit == None:
                prevfit = yfit

            if N.abs(resid[-1]) < eps:
                inext = (inext+1)%n
                run.append(poly[inext])
                runi.append(inext)
                continue
            elif N.abs(resid[0]) < eps:
                iprev = (iprev-1)%n
                run.insert(0,poly[iprev])
                runi.insert(0,iprev)
                continue
            else:
                break

        if yfit == None: continue
        c = N.transpose(yfit)
        a,b = trim(resid)
        #print i,a,b
        start,end = c[a],c[b-1]

        if len(runi[a:b])==0: continue
        
        allruns.append(runi[a:b])
        out.append([start,end])
        runl.append(len(x[fitii]))
        fitstats.append((fitl,fitstd))

    if len(allruns)==0:
        return [],(0,0),0
    
    #l = [len(a) for a in allruns]
    i = N.argmax(runl)
    #print "length",runl[i]
    #print "fit stats n=%i  sig=%f"%fitstats[i]
    return allruns[i],out[i],runl[i]
    
    

def simplify(poly,plot=False):
    """ """
    poly = removedups(poly)  # remove duplicate vertices
    poly = N.round(poly)     # fix to integer pixels
    x,y = N.transpose(poly)

    while True:
        runi,(start,end),l = findruns(poly)

        if l < 3:
            break
        #poly[runi[0]] = start
        #poly[runi[-1]] = end
        #print 'ind',runi[0],runi[-1]
        #print 'start end',start,end
        #print 'old      ',poly[runi[0]],poly[runi[-1]]
        #pylab.plot([poly[runi[0]][0],start[0]],[poly[runi[0]][1],start[1]],"-")
        #pylab.plot([poly[runi[-1]][0],end[0]],[poly[runi[-1]][1],end[1]],"-")
        #print runi,start,end,l
        poly[runi[0]] = start
        poly[runi[-1]] = end

        poly2 = []
        #print 'runi',runi
        for j in range(len(poly)):
            if j in runi[1:-1]: continue
            poly2.append(poly[j])

        if plot:
            pylab.figure()
            x1,y1 = N.transpose(poly)
            pylab.plot(x,y,".")
            x,y = N.transpose(poly2)
            pylab.plot(x,y,"o-")
            pylab.show()
        poly = poly2
        

    return poly


def test():
    import pickle,pylab
    path = 'polysave.pickle'
    if len(sys.argv)>1:
        path = sys.argv[1]
    poly = pickle.load(file(path))
    x,y = N.transpose(poly)
    pylab.plot(x,y,"r.")


    pout = simplify(poly)
    x,y = N.transpose(pout)
    pylab.plot(x,y,"bo-")

    print 'reduction %i => %i'%(len(poly),len(pout))
    
    pylab.show()


if __name__=="__main__":
    test()
                
            
