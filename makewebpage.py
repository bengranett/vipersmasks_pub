import sys,os
import numpy as N

version=''
regionsdir = 'regions%s'%version
imagesdir = 'images'
smallimagesdir = 'smallimages'


def printlist(plist,field='W1'):
    for p in plist:
        if not p.startswith(field): continue
        print "<tr><td>%s</td>"%p,
    
        for q in quads:
            path = '%s/%s_%s.gif'%(imagesdir,p,q)
            thumb = '%s/%s_%s_s.gif'%(smallimagesdir,p,q)
            print "<td><a href=\"%s\"><img src=\"%s\"></a></td>"%(path,thumb),

        print "</tr>"


def printheader():
    print """<html>
<head>
<title>VIPERS field masks</title>
<style type="text/css">
p {width:800px;}
</style>
</head>
<body>
<h1>VIPERS field masks</h1>
<p>
<h2>W1</h2>
<a href='area_w1.pdf'><img src='area_w1.png'></a>
<h2>W4</h2>
<a href='area_w4.pdf'><img src='area_w4.png'></a>

<hr>
<h2>Algorithm</h2>

<p> The masks were constructed from the pre-imaging data by running an
image analysis routine that identifies 'good' regions.  First, a
polygon is defined that traces the edge of the image.  The mean and
variance of the pixels are computed in small patches at the vertices
of the polygon.  These measurements are compared to the statistics at
the center of the image.  The vertices of the polygon are then
iteratively moved inward toward the center until the statistics along
the boundary are within an acceptable range.</p>

<p>The boundary that results from this algorithm is used as the basis
for the field geometry.  The polygon is next simplified to reduce the
vertex count; short segments that are nearly co-linear are replaced by
long segments.</p>

<p>Lastly, each mask was examined by eye. Features in the masks due to
stars at the edge of an image were removed, wiggly segments were
straightened and artifacts due to moon reflections were corrected.
The software to do all this is linked below.</p>

<p>To report any problems you find in the plots below, contact Ben
Granett, ben.granett@brera.inaf.it</p>

<hr> <h2>All pointings</h2>
<p>The images here are displayed with the 'zscale' color mapping in
DS9.  </p>
<p>Color codes: the field mask is shown in red.  Targets that are in
database version 1.1 are circled in cyan.  Targets with zflag < 1 are
circled in magenta.</p>

<p> Fields:
<a href='#W1'>W1</a> <a
href='#W4'>W4</a>
</p>

<hr>
<h2>Software</h2>
<ul><li><a href='vipersmask.tar.gz'>vipersmask.tar.gz</a></li></ul>
<hr>
"""





plist = []
cache = {}
for f in os.listdir(regionsdir):
    if not f.endswith("reg"): continue
    if not f.startswith("mask"): continue
    tag = f[5:13]
    pointing = tag[:6]
    quad = tag[6:8]

    if cache.has_key(pointing): continue
    cache[pointing]=1
    plist.append(pointing)

plist.sort()

quads = ['Q1','Q2','Q3','Q4']

printheader()

print "<h2 id=\"W1\">W1</h2>"
print "<table>"
printlist(plist,'W1')
print "</table>"

print "<h2 id=\"W4\">W4</h2>"
print "<table>"
printlist(plist,'W4')
print "</table>"

 
print """<hr>
</body>
</html>
"""

