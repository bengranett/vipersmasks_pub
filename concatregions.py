import sys,os
import numpy as N

version = ''

def go(field='W4',rootdir='regions%s'%version,outdir='web%s'%version):
    stuff = "global color=red\nFK5\n"
    data = []
    pointings = []
    for path in os.listdir(rootdir):
        if not path.endswith(".reg"): continue
        if not path.startswith("mask"): continue
        tag = path.split("_")[1]
        p = tag[:6]
        q = tag[6:8]
        if not p.startswith(field): continue
        print p,q
        pointings.append('%s %s'%(p,q))
        
        poly = file("%s/%s"%(rootdir,path)).read().split("\n")[-2]
        data.append(poly)

    pointings = N.array(pointings)
    data = N.array(data)
    order = N.argsort(pointings)
    pointings = pointings[order]
    data = data[order]

    # make sure formatting is uniform
    data2=[]
    for d in data:
        assert(d.startswith('polygon'))
        d = d.replace("("," ")
        d = d.replace(")"," ")
        d = d.replace(","," ")
        w = d.split()[1:]
        d = 'polygon(%s)'%(",".join(w))
        data2.append(d)


    outfile = '%s/specmask_%s_all.reg'%(outdir,field)
    out = file(outfile,'w')

    print >>out, stuff
    for i in range(len(pointings)):
        print >> out, '# %s'%pointings[i]
        print >> out, data2[i]


    out.close()
        


if __name__=="__main__":
    go('W4')
    go('W1')
