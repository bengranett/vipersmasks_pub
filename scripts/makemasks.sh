#!/bin/bash

pixelize -m1e-5 -Ps0,11 `masklist.py -f W1 -b manglemasks/mask_%s.reg.mangle` masks1.1_W1.pix
balkanize -m1e-5 masks1.1_W1.pix masks1.1_W1.balk
unify  -m1e-5 masks1.1_W1.balk masks1.1/masks1.1_W1.mangle
rm masks1.1_W1.pix masks1.1_W1.balk
plotmask.py --save plots/masks1.1_W1.png masks1.1/masks1.1_W1.mangle

pixelize -m1e-5 -Ps0,11 `masklist.py -f W4 -b manglemasks/mask_%s.reg.mangle` masks1.1_W4.pix
balkanize -m1e-5 masks1.1_W4.pix masks1.1_W4.balk
unify  -m1e-5 masks1.1_W4.balk masks1.1/masks1.1_W4.mangle
rm masks1.1_W4.pix masks1.1_W4.balk
plotmask.py --save plots/masks1.1_W4.png masks1.1/masks1.1_W4.mangle



# cut out the photometry mask from the field mask
echo 0 > zero.weight
weight -zzero.weight photomasks_mangle/CFHTLS_W1_i_T0005_Jean.reg.mangle CFHTLS_W1.w
pixelize -m1e-5 -Ps0,11 `masklist.py -f W1 -b manglemasks/mask_%s.reg.mangle` masks1.1_W1.pix
balkanize -m1e-5  masks1.1_W1.pix CFHTLS_W1.w  starsW1.balk
unify  -m1e-5  starsW1.balk masks1.1_cut/masks1.1_W1_photcut.mangle
plotmask.py --save plots/masks1.1_W1_photcut.png  masks1.1_cut/masks1.1_W1_photcut.mangle
rm CFHTLS_W1.w masks1.1_W1.pix starsW1.balk


weight -zzero.weight photomasks_mangle/CFHTLS_W4_i_T0005_Jean.reg.mangle CFHTLS_W4.w
pixelize -m1e-5 -Ps0,11 `masklist.py -f W4 -b manglemasks/mask_%s.reg.mangle` masks1.1_W4.pix
balkanize -m1e-5  masks1.1_W4.pix CFHTLS_W4.w  starsW4.balk
unify  -m1e-5  starsW4.balk masks1.1_cut/masks1.1_W4_photcut.mangle
plotmask.py --save plots/masks1.1_W4_photcut.png  masks1.1_cut/masks1.1_W4_photcut.mangle
rm CFHTLS_W4.w masks1.1_W4.pix starsW4.balk
rm zero.weight