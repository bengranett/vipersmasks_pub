import numpy as N
import sys
import pylab


def apply(fit,x,y):
    """ """
    mx,my,evec = fit
    c = [x-mx,y-my]

    xt,yt = N.dot(evec,c)
    resid = yt

    inv = N.linalg.inv(evec)
    inv = N.dot(inv,N.diag([1,0]))

    xo,yo = N.dot(inv,[xt,yt])
    xo += mx
    yo += my

    return (xo,yo),resid

def fit(xin,yin):
    mx = N.mean(xin)
    my = N.mean(yin)
    x = xin-mx
    y = yin-my
    C = N.cov(x,y)

    eval,evec = N.linalg.eig(C)
    evec = N.transpose(evec)
    order = N.argsort(eval)[::-1]
    evec = evec[order]
    eval = eval[order]

    f = (mx,my,evec)

    yfit,r = apply(f,xin,yin)
    
    return f,yfit,r

def fitline(x,y, usefit=None, iterations=1, cut=2, eps=1):
    """ """
    ii = N.where(x==x)
    #pylab.figure()
    for loop in range(iterations+1):
        if len(x[ii])<2:
            return None,None,None,None

        if loop==0 and usefit != None:
            f,sig = usefit
            yf,r = apply(f,x,y)
            
        else:
            f,yf,r = fit(x[ii],y[ii])
            yf,r = apply(f,x,y)
            sig = N.std(r[ii])

        #print ">>> loop",loop
        #print x[ii],y[ii]
        #print N.mean(r[ii]),N.median(r[ii])
        #sig = N.std(r[ii])
        #sig = 10.
        if sig < eps:
            sig=eps
        l = len(x[ii])
        #m = N.median(r[ii])
        m = 0.0
        ii = N.where(N.abs(r-m)/sig < cut)

        #print N.abs(r-m)/sig
        #pylab.figure()
        #pylab.plot(x,y,".")
        #a,b = yf
        #pylab.plot(a,b)

        
        if len(x[ii])==l:
            break

    #pylab.show()


    return f,yf,r,ii

def test():
    pylab.subplot(111,aspect='equal')
    slope = .01
    b = 2
    x = N.arange(10)
    y =  slope*x + b + N.random.normal(0,.1,len(x))

    y,x = x,y
    x[3] = 10

    pylab.plot(x,y,"o")

    f,yf,r = fitline(x,y)

    pylab.plot(yf[0],yf[1])
    pylab.figure()
    pylab.plot(r)


    pylab.show()

if __name__=='__main__':
    test()
