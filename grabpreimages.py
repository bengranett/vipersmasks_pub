import sys,os
import urllib2
import disp2

from HTMLParser import HTMLParser

class MyHTMLParser(HTMLParser):

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for k,v in attrs:
                if k=='href':
                    self.filelist.append(v)



webpage = 'http://195.221.212.82/viperscgi/obsUWdb/vipers_pts.pl'


def download(pointing,untar=False):
    """ """
    try:
        assert(pointing.startswith('W1') or pointing.startswith('W4'))
        assert(len(pointing)==6)
    except:
        print "> invalid pointing name: %s"%pointing
        return
        
    root = "data"
    dir = "%s/%s"%(root,pointing)
    path0 = "%s.tar.gz"%(pointing)
    path = "%s/%s.tar.gz"%(root,pointing)

    if os.path.exists(path) or os.path.exists(dir):
        print "> already got %s"%pointing
        return
    
    url = 'http://195.221.212.82/vipers/pre_images/%s'%path0

    cmd = 'wget --http-user=none --http-password=none %s -O %s'%(url,path)
    print cmd
    
    r=0
    r=os.system(cmd)

    if r!=0:
        print "> wget error",r
        #sys.exit(r)
        return

    if untar:
        if not os.path.exists(dir):
            os.system('mkdir %s'%dir)

        r = os.system('tar xvzf %s -C %s'%(path,dir))
        if r!=0:
            print "> tar error"
            sys.exit(r)



def listpointings(fields=['W1','W4']):
    parser = MyHTMLParser()
    parser.filelist = []

    for field in fields:
        tmpfile = 'tmp/pointinglist_%s.html'%field
        if not os.path.exists(tmpfile):
            postdata = 'field=%s'%field
            cmd = 'wget --http-user=none --http-password=none --post-data=%s -O %s %s'%(postdata,tmpfile,webpage)
            r = os.system(cmd)
            if not r==0:
                print cmd
                print "wget error",r
                sys.exit()

        html = file(tmpfile,'r').read()
        parser.feed(html)

    pointinglist = []
    for f in parser.filelist:
        if f.endswith('.tar.gz'):
            pointing = f.split("/")[-1]
            pointing = pointing.split(".")[0]
            if disp2.checkpointing(pointing):
                pointinglist.append(pointing)

    return pointinglist


if __name__=="__main__":
    if sys.argv[1] == 'list':
        plist = listpointings()
        print "found pointings"
        print " ".join(plist)
    elif sys.argv[1]=='download':
        pointings = sys.argv[2:]
        
        for p in pointings:
            download(p)
    else:
        print "give me a command"
