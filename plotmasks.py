import pylab
import matplotlib
import numpy as N

version = ''

colors = ['r','b','y','c']

def plotfields(path):
    """ """
    label=None
    ax = pylab.gca()
    for line in file(path):
        line = line.strip()
        if line.startswith("#"):
            label = line.split('#')[1].strip()
            if label.startswith('W'):
                label = label[3:]
                label = label.replace(' ','\n')
            print label
            continue
        if not line.startswith('polygon'): continue

        line = line.replace("("," ")
        line = line.replace(")"," ")
        line = line.replace(","," ")
        
        
        i = line.find('#')
        if i > -1:
            w = line[:i].split()
        else:
            w = line.split()

        c = [float(v) for v in w[1:]]
        x = N.take(c,N.arange(0,len(c),2))
        y = N.take(c,N.arange(1,len(c),2))
        xy = N.transpose([x,y])

        color = 'None' 
        if not label==None:
            color = colors[int(label.split()[0])%4]
        
        p = matplotlib.patches.Polygon(xy,facecolor=color,alpha=0.2)
        pylab.plot(x,y,ls='None')
        if label != None:
            pylab.text((x.min()+x.max())/2.,(y.min()+y.max())/2.,label,fontsize=20,
                       horizontalalignment='center',
                       verticalalignment='center')
        ax.add_artist(p)

def plotstars(path):
    ax = pylab.gca()
    for line in file(path):
        line = line.strip()
        if not line.startswith('polygon'): continue
        i,j = line.find("("),line.find(")")
        line = line[i+1:j]

        w = line.split(",")

        c = [float(v) for v in w]

        x = N.take(c,N.arange(0,len(c),2))
        y = N.take(c,N.arange(1,len(c),2))

        xy = N.transpose([x,y])

        color = 'g'
        
        p = matplotlib.patches.Polygon(xy,facecolor=color,alpha=0.2)
        pylab.plot(x,y,ls='None')


def gow1(rootdir='web%s'%version):
    pylab.figure(figsize=(55,8))
    pylab.subplot(111,aspect=N.cos(4*N.pi/180))
    pylab.subplots_adjust(left=0.02,right=.99,bottom=0,top=1)
    plotfields('%s/specmask_W1_all.reg'%rootdir)
    x = pylab.xlim()
    pylab.xlim(x[1],x[0])
    pylab.xlabel('Right ascension')
    pylab.ylabel('Declination')
    pylab.savefig('%s/area_w1.pdf'%rootdir)
    pylab.savefig('%s/area_w1.png'%rootdir,dpi=20)

def gow4(rootdir='web%s'%version):
    pylab.figure(figsize=(40,8))
    pylab.subplot(111,aspect=1)
    pylab.subplots_adjust(left=0.02,right=.99,bottom=0,top=1)

    #plotstars('/home/ben/data/vipers/VIPERS_masks/masks/CFHTLS_W4_i_T0005_Jean.reg')
    plotfields('%s/specmask_W4_all.reg'%rootdir)
    x = pylab.xlim()
    pylab.xlim(x[1],x[0])

    pylab.xlabel('Right ascension')
    pylab.ylabel('Declination')
    pylab.savefig('%s/area_w4.pdf'%rootdir)
    pylab.savefig('%s/area_w4.png'%rootdir,dpi=20)



if __name__=="__main__":
    gow1()
    gow4()
