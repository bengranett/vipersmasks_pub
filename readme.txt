
These masks and code were prepared by Ben Granett  ben.granett@brera.inaf.it

Software requirements:
python
numpy
matplotlib
scipy
pyfits
ds9 and ds9 python bindings http://hea-www.harvard.edu/saord/ds9/pyds9/
kapteyn astro python library http://www.astro.rug.nl/software/kapteyn/
wget is used to download data
imagemagick 


disp2.py          makes field masks and generates ds9 region files
findedges.py      the code used by disp to find the image boundaries

concatregions.py  concatenates ds9 region files

editregion.py     opens the regions for editing in ds9.

filterim.py       not used

grabpreimages.py  download pre-imaging fits data from the server.
makewebpage.py    builds web page
plotmasks.py      generates a plot of the fields
showallmasks.py   not used



directory structure:
 data     pre-imaging data goes here
 regions  region files are put here by disp2.py
 web      a web site is generated here to show the results.
 tmp      a place for temporary files


catalogs and auxilliary files
readme.txt    this file
data_1.1/W1spec.fits        VIPERS spectroscopic catalogs version 1.1
data_1.1/W4spec.fits
data_1.1/pointings_1.1.txt  fields in release 1.1
